// const userOld = require('../models/UserOld');
const userModel = require('../models/User');
const orderModel = require('../models/Order');
// const productModel = require('../models/Product');
const shoppingCartModel = require('../models/ShoppingCart');
const saveForLaterModel = require('../models/SaveForLater');

//username validation
const validateUsername = (username) => {
    const regExp = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,8})$/;
    if (!regExp.exec(username)) {
        return false;
    }
    return true;
}

const validatePassword = (password) => {
    const regExp = /^[\w_-]{6,16}$/;
    // const regExp = /^{6,16}$/;
    if (!regExp.exec(password)) {
        return false;
    }
    return true;
}

//GET /check if a username is already exist (用于表单直接显示)
exports.available = async (req, res) => {
    try {
        const username = req.body.username;
        const user = await userModel.readByUsername(username);
        if (!user) {
            req.status(200).json({ message: `username available.` });
        } else {
            req.status(422).json({ errorMessage: `username already exists.` });
        }
    } catch (err) {
        res.status(500).json({ errorMessage: `Internal Server Error.` });
    }
}

// post 都要用正则表达式
//POST  /user register
exports.register = async (req, res) => {
    try {
        const username = req.body.username;
        const password = req.body.password;
        const forename = req.body.forename;
        const surname = req.body.surname;
        const mobile = req.body.mobile;
        const contactPreference = req.body.contactPreference;
        if (!validateUsername(username)) {
            res.status(422).json({ errorMessage: `Invalid username.` });
        } else if (!validatePassword(password)) {
            res.status(422).json({ errorMessage: 'You password can only contain numbers, underscore, dash and uppercase and lowercase letters, and the length of the password have to between 6 and 16.' })
        } else if (await userModel.readByUsername(username)) {
            res.status(409).json({ errorMessage: `username ${username} already taken.` });
        } else {
            const u = await userModel.create({ username, password, forename, surname, mobile, contactPreference });
            res.status(201).json({ message: `${username} created successfully.` });
        }
    } catch (err) {
        res.status(500).json({ errorMessage: `Internal Server Error.` });
    }
}

// POST  /user login
exports.login = async (req, res) => {
    try {
        const { username, password } = req.body;
        const user = await userModel.readByUsername(username);
        if (!user) {
            res.status(401).json({ errorMessage: `username does not exist.` });
        } else if (password == user.password) {
            req.session.regenerate(function (err) {
                if (err) {
                    return res.status(401).json({ ret_msg: 'failed.' });
                }
                req.session.uid = user.id;
                res.status(200).json({ message: `Hello ${user.forename}` });
            });
        } else if (password != user.password) {
            res.status(401).json({ errorMessage: `Incorrect password.` });
        } else {
            res.status(401).json({ errorMessage: `Incorrect credentials.` });
        }
    } catch (err) {
        res.status(500).json(err);
        res.status(500).json({ errorMessage: `Internal Server Error.` });
    }
}

//DELETE /user signout
exports.signOut = async (req, res) => {
    try {
        req.session.destroy(function (err) {
            if (err) {
                res.status(401).json({ errorMessage: `Signout failed.` });
            } else {
                res.clearCookie('skey');
                // 备注：这里用的 session-file-store 在destroy 方法里，并没有销毁cookie
                // 所以客户端的 cookie 还是存在，导致的问题 --> 退出登陆后，服务端检测到cookie
                // 然后去查找对应的 session 文件，报错
                // session-file-store 本身的bug
                res.status(204).end();
                // res.redirect('/');
            }
        });
    } catch (err) {
        res.status(500).json({ errorMessage: `Internal server Error.` });
    }
}

//GET /get user profile
exports.getProfile = async (req, res) => {
    try {
        const { uid } = req.session;
        if (!uid) {
            res.status(401).json({ message: 'Not authorized' });
        }
        const user = await userModel.read(uid);
        res.status(200).json(user);
    } catch (err) {
        res.status(500).json(err);
    }
}

//PUT /update profile (except password)
exports.updateBasicProfile = async (req, res) => {
    try {
        const uid = req.session.uid; //check authorization
        const updates = req.body;
        if (!uid) {
            res.status(500).json({ errorMessage: 'No authorization.' });
        } else {
            const user = await userModel.updateBasicProfile(uid, updates);
            res.status(200).json({ user });
        }
    } catch (err) {
        res.status(500).json({ errorMessage: 'Internal Server Error.' });
    }
}

//POST /add to address
exports.addAddress = async (req, res) => {
    try {
        const { uid } = req.session;
        if (!uid) {
            res.status(401).json({ message: 'Not authorized' });
        }
        const address = req.body;
        const user = await userModel.addAddress(uid, address);
        res.status(201).json(user);
    } catch (err) {
        res.status(500).json(err);
    }
}

//PUT /update address
exports.updateAddress = async (req, res) => {
    try {
        const { uid } = req.session;
        if (!uid) {
            res.status(401).json({ message: 'Not authorized' });
        }
        const updates = req.body;
        const user = await userModel.updateAddress(uid, updates._id, updates);
        res.status(200).json(user);
    } catch (err) {
        res.status(500).json(err);
    }
};

//delete /delete address
exports.deleteAddress = async (req, res) => {
    try {
        const { uid } = req.session;
        if (!uid) {
            res.status(401).json({ message: 'Not authorized' });
        }
        const { addressId } = req.body;
        const user = await userModel.deleteAddress(uid, addressId);
        res.status(204).end();
    } catch (err) {
        res.status(500).json(err);
    }
}

//POST /add payment 
exports.addPayment = async (req, res) => {
    try {
        const { uid } = req.session;
        if (!uid) {
            res.status(401).json({ message: 'Not authorized' });
        }
        const payment = req.body;
        const user = await userModel.addPayment(uid, payment);
        res.status(200).json(user);
    } catch (err) {
        res.status(500).json(err);
    }
}

// POST  /update payment
exports.updatePayment = async (req, res) => {
    try {
        const { uid } = req.session;
        if (!uid) {
            res.status(401).json({ message: 'Not authorized' });
        }
        const updates = req.body;
        const user = await userModel.updatePayment(uid, updates._id, updates);
        res.status(200).json(user);
    } catch (err) {
        res.status(500).json(err);
    }
}

//delete /delete payment
exports.deletePayment = async (req, res) => {
    try {
        const { uid } = req.session;
        if (!uid) {
            res.status(401).json({ message: 'Not authorized' });
        }
        const { paymentId } = req.body;
        const user = await userModel.deletePayment(uid, paymentId);
        res.status(204).end();
    } catch (err) {
        res.status(500).json(err);
    }
}


// Update password (encryption)

//shopping cart

exports.addOrUpdateShoppingCart = async (req, res) => {
    try {
        const { uid } = req.session;
        const { op } = req.query; // operation, increment or modify number
        const { product_id, number } = req.body;
        switch (op) {
            case 'inc': {
                const shoppingCart = await shoppingCartModel.addOrIncrement(uid, product_id, number);
                res.status(200).json(shoppingCart);
                break;
            }
            case 'mod': {
                const shoppingCart = await shoppingCartModel.addOrModifyNumber(uid, product_id, number);
                res.status(200).json(shoppingCart);
                break;
            }
            default:
                res.status(400).json({ errorMessage: 'Invalid operation.' });
        }
    } catch (err) {
        res.status(500).json({ errorMessage: 'Internal Server Error' });
    }
}

//Get /get a user's shopping cart
exports.getShoppingCart = async (req, res) => {
    try {
        const uid = req.session.uid;
        if (!uid) {
            res.status(500).json({ errorMessage: 'No authorization.' });
        }
        const shoppingCart = await shoppingCartModel.readByUid(uid);
        res.status(200).json( {shoppingCart} );
    } catch (err) {
        res.status(500).json({ errorMessage: `Internal Server Error.` });
    }
}

//DELETE /delete from shopping cart
exports.deleteFromShoppingCart = async (req, res) => {
    try {
        const uid = req.session.uid;
        if (!uid) {
            res.status(500).json({ errorMessage: 'No authorization.' });
        }
        const { product_id } = req.body;
        const shoppingCart = await shoppingCartModel.remove(uid, product_id);
        res.status(204).end();
    } catch (err) {
        res.status(500).json({ errorMessage: `Internal Server Error.` });
    }
}

//POST /move product from shopping cart to save for later
exports.moveToSaveForLater = async (req, res) => {
    try {
        const uid = req.session.uid;
        if (!uid) {
            res.status(500).json({ errorMessage: 'No authorization.'});
        }
        const { product_id, number } = req.body;
        const saveForLater = await saveForLaterModel.addOrIncrement(uid, product_id, number);
        const shoppingCart = await shoppingCartModel.remove(uid, product_id);
        res.status(200).json(shoppingCart);
    } catch (err) {
        res.status(500).json({ errorMessage: `Internal Server Error.` });
    }
}

//POST /add or update save for later
exports.addOrUpdateSaveForLater = async (req, res) => {
    try {
        const { uid } = req.session;
        const { op } = req.query; // operation, increment or modify number
        const { product_id, number } = req.body;
        switch (op) {
            case 'inc': {
                const saveForLater = await saveForLaterModel.addOrIncrement(uid, product_id, number);
                res.status(200).json( saveForLater );
                break;
            }
            case 'mod': {
                const saveForLater = await saveForLaterModel.addOrModifyNumber(uid, product_id, number);
                res.status(200).json( saveForLater );
                break;
            }
            default:
                res.status(400).json({ errorMessage: 'Invalid operation.' });
        }
    } catch (err) {
        res.status(500).json({ errorMessage: 'Internal Server Error' });
    }
}

//Get /get a user's save for later
exports.getSaveForLater = async (req, res) => {
    try {
        const uid = req.session.uid;
        if (!uid) {
            res.status(500).json({ errorMessage: 'No authorization.' });
        }
        const saveForLater = await saveForLaterModel.readByUid(uid);
        res.status(200).json( saveForLater );
    } catch (err) {
        res.status(500).json({ errorMessage: `Internal Server Error.` });
    }
}

//DELETE  /delete from save for later
exports.deleteFromSaveForLater = async (req, res) => {
    try {
        const uid = req.session.uid;
        if (!uid) {
            res.status(500).json({ errorMessage: 'No authorization.' });
        }
        const { product_id } = req.body;
        await saveForLaterModel.remove(uid, product_id);
        res.status(204).end();
    } catch (err) {
        res.status(500).json({ errorMessage: `Internal Server Error.` });
    }
}

//POST /move product from save for later to shopping cart
exports.moveToShoppingCart = async (req, res) => {
    try {
        const uid = req.session.uid;
        if (!uid) {
            res.status(500).json({ errorMessage: 'No authorization.'});
        }
        const { product_id, number } = req.body;
        const shoppingCart = await shoppingCartModel.addOrIncrement(uid, product_id, number);
        const saveForLater = await saveForLaterModel.remove(uid, product_id);
        res.status(200).json(shoppingCart);
    } catch (err) {
        res.status(500).json({ errorMessage: `Internal Server Error.` });
    }
}

//POST /submit an order
exports.addToOrderHistory = async (req, res) => {
    try {
        const uid = req.session.uid;
        if (!uid) {
            res.status(500).json({ errorMessage: 'No authorization.' });
        }
        const orderInfo = {
            ...req.body,
            uid,
            date: new Date(),
        }
        await orderModel.create(orderInfo);
        await shoppingCartModel.clearShoppingCart(uid);
        res.status(200).json({message: 'Order submitted successfully'});
    } catch (err) {
        res.status(500).json({ errorMessage: `Internal Server Error.` });
    }
}

//Get /get order list 
exports.getOrderHistory = async (req, res) => {
    try {
        const uid = req.session.uid;
        if (!uid) {
            res.status(401).json({ errorMessage: 'Not Authorized.' });
        } else {
            const orders = await orderModel.getOrderHistory(uid);
            res.status(200).json( orders );
        }
    } catch (err) {
        res.status(500).json({ errorMessage: 'Internal Server Error.' });
    }
}

//DELETE /delete from order history (delete order from orderModel? not need)
exports.deleteFromOrderHistory = async (req, res) => {
    try {
        const uid = req.session.uid;
        if (!uid) {
            res.status(500).json({ errorMessage: 'No authorization.' });
        }
        const order_id = req.params.order_id;
        const orderHistory = await orderModel.remove(uid, order_id);
        res.status(200).json(orderHistory);
    } catch (err) {
        res.status(500).json({ errorMessage: `Internal Server Error.` });
    }
}

