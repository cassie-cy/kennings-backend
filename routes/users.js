var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/:username', (req, res, next) => {
  res.status(200).json({ username: req.params.username });
});

module.exports = router;
