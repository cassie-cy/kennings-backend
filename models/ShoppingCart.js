const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ShoppingCart = mongoose.model('shoppingCart', new Schema({
    uid: {
        type: Schema.Types.ObjectId,
        ref: 'user',
        index: true,
    },
    product: {
        type: Schema.Types.ObjectId,
        ref: 'product',
        index: true,
    },
    number: Number,
}));

exports.addOrIncrement = (uid, product_id, increment) => ShoppingCart
    .findOneAndUpdate(
        { uid, product: product_id },
        { '$inc': { number: increment } },
        { new: true, upsert: true }).exec();

exports.addOrModifyNumber = (uid, product_id, number) => ShoppingCart
    .findOneAndUpdate({
        uid,
        product: product_id,
    }, { number }, { new: true, upsert: true })
    .exec();

exports.readByUid = (uid) => ShoppingCart
    .find({ uid })
    .populate('product')
    .select('product number')
    .exec();

exports.remove = (uid, product) => ShoppingCart
    .remove({ uid, product }).exec();

exports.clearShoppingCart = (uid) => ShoppingCart
    .remove({ uid }).exec();