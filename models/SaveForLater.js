const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const SaveForLater = mongoose.model('saveForLater', new Schema({
    uid: {
        type: Schema.Types.ObjectId,
        ref: 'user',
    },
    product: {
        type: Schema.Types.ObjectId,
        ref: 'product',
    },
    number: Number,
}));

exports.addOrIncrement = (uid, product_id, increment) => SaveForLater
    .findOneAndUpdate(
        { uid, product: product_id },
        { '$inc': { number: increment } },
        { new: true, upsert: true }).exec();

exports.addOrModifyNumber = (uid, product_id, number) => SaveForLater
    .findOneAndUpdate({
        uid,
        product: product_id,
    }, { number }, { new: true, upsert: true })
    .exec();

exports.readByUid = (uid) => SaveForLater
    .find({ uid })
    .populate('product')
    .select('product number')
    .exec();

exports.remove = (uid, product) => SaveForLater
    .remove({ uid, product }).exec();
