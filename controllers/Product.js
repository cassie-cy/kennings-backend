const productModel = require('../models/Product');

exports.getProductList = async (req, res) => {
    try {
        const category = req.params.category;
        const products = await productModel.getProductCategory(category);
        res.status(200).json({ price, name, picture } = products);
    } catch (err) {
        res.status(500).json({ errorMessage: 'Internal Server Error.' });
    }
}
// order history get product info 
exports.getProductsInfo = async (req, res) => {
    try {
        const id = req.params.product_id;
        const productsInfo = await productModel.getProductsInfo(id);
        res.status(200).json({ productsInfo });
    } catch (err) {
        res.status(500).json({ errorMessage: 'Internal Server Error.' });
    }
}

//get one product info
exports.getProductInfo = async (req, res) => {
    try {
        const id = req.params.product_id;
        const productInfo = await productModel.getProductInfo(id);
        res.status(200).json({ productInfo });
    } catch (err) {
        res.status(500).json({ errorMessage: 'Internal Server Error.' });
    }
}

exports.search = async (req, res) => {
    try {
        const { keyword } = req.query;
        if (!keyword) {
            res.status(400).json({ errorMessage: 'Bad Request.' });
        } else {
            const products = await productModel.search(keyword);
            res.status(200).json(products);
        }
    } catch (err) {
        res.status(500).json({ errorMessage: 'Internal Server Error.' });
    }
}