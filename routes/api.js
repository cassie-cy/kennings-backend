const express = require('express');
const router = express.Router();
const userController = require('../controllers/user');
const productController = require('../controllers/Product');
// const userExperiment = require('../models/User');

// router.get('/', (req, res))

//login and register
// User register
router.post('/user/register', userController.register);

//User login
router.post('/user/login', userController.login);

//User signout
router.delete('/user/signout', userController.signOut);

//Get user profile
router.get('/user/account', userController.getProfile);

//Update user profile(except password), requires authronization
router.put('/user/account-settings', userController.updateBasicProfile);

//Add user address
router.post('/user/address', userController.addAddress);

//PUT /update address
router.put('/user/address', userController.updateAddress);

//Delete user address
router.delete('/user/address', userController.deleteAddress);

//Add user payment
router.post('/user/payment', userController.addPayment);

//PUT update payment
router.put('/user/payment', userController.updatePayment);

//Delete user payment
router.delete('/user/payment', userController.deletePayment);

// Update password (encryption)
// router.patch('/user', userController.updatePassword);


// shopping
router.post('/user/shoppingcart', userController.addOrUpdateShoppingCart);
//Add to shoppingCart, requires authorization

//Get a user's shoppingCart
router.get('/user/shoppingcart', userController.getShoppingCart);

//Delete a product from a user's shoppingCart, requires authorization
router.delete('/user/shoppingcart', userController.deleteFromShoppingCart);

//POST /move product from shopping cart to save for later
router.post('/user/shoppingcart/move', userController.moveToSaveForLater);

// add or update save for later
router.post('/user/saveforlater', userController.addOrUpdateSaveForLater);

//Get a user's save for later list
router.get('/user/saveforlater', userController.getSaveForLater);

//Delete a product from a user's save for later list
router.delete('/user/saveforlater', userController.deleteFromSaveForLater);

//POST /move product from save for later to shopping cart
router.post('/user/saveforlater/move', userController.moveToShoppingCart);

//Submit an order(orderModel and userModel)
router.post('/user/orderhistory', userController.addToOrderHistory);

//Get order history
router.get('/user/orderhistory', userController.getOrderHistory);

//Delete from order history
router.delete('/user/orderHistory/:order_id', userController.deleteFromOrderHistory);



//Get product information
//Get category list
router.get('/products/:category', productController.getProductList);

//Get product specific information
router.get('/products/:product_id', productController.getProductsInfo);

//Get one  product info
router.get('/product/:product_id', productController.getProductInfo);

router.get('/search', productController.search);

module.exports = router;
