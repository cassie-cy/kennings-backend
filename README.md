# Kennings Backend

Backend for Kennings website.

## Requirements

To run the backend, you have to first install the following applications:

- MongoDB (Tested with v3.6.3)
- Redis (Tested with v4.0.9)

## How to setup MongoDB

The database need to be filled with products information before running the application. Data stored in the database should conform the format of the data models in [./models](./models).

An example of Product information is shown as follows:

```javascript
{
    name: "Sawn Treated Timber Softwood carcassing 19mm x 38mm x 3.6m",
    details: "",
    price: 3.14,
    quatity: 100,
    code: "000001",
    tecInfo: "",
    review: [],
    category: "timber",
    picture: "/images/timber1.jpg",
}
```

Note: The `required` and `index` fields in the data models are not part of the data, they are parameters for the MongoDB middleware used in the project. Fields with `required: true` flag can not be left empty when filling data in the database. 

## How to Install

To install the packages, in the project directory, you can run:

`npm install`

## How to Run

### Run directly

Execute the following command in the project folder:

`npm start`

### Run with debuggers

To run the frontend with debuggers, run:

`npm run debug`

Then the Node.js process will listen for a debugging client, which by default listens to port `127.0.0.1:9229`.

You can use the debugger of your choice, e.g. Chrome DevTools.