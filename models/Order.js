const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Order = mongoose.model('order', new Schema({
    products: {
        type: [{
            product: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'product'
            },
            number: Number,
        }],
        required: true,
    },
    uid: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user',
        required: true,
    },
    date: {
        type: Date,
        required: true,
    },
    address: {
        type: {
            forename: String,
            surname: String,
            country: String,
            flatNumber: String,
            streetAddress: String,
            address2: String,
            city: String,
            postcode: String,
        },
        // required: true,
        default: Object
    },
    payment: {
        type: {
            cardNumber: String,
            expiryDate: String,
            billingAddress: {
                forename: String,
                surname: String,
                country: String,
                flatNumber: String,
                streetAddress: String,
                address2: String,
                city: String,
                postcode: String,
            },
        },
        // required: true,
        default: Object
    },
    totalPrice: {
        type: Number,
        required: true,
    },
    status: {
        type: String,
        required: true,
    }
}));

exports.getOrderHistory = (uid) => {
    return Order.find({ uid })
        .populate('products.product', 'name picture price')
        .select('products date totalPrice')
        .exec();
}

//get an order
exports.readByOrderId = (oid) => {
    return Order.findById(oid).exec();
}

//create an order
/**
 * Create an order
 * @param {Object} orderInfo    information used to create the order record
 */
exports.create = (orderInfo) => {
    return Order.create(orderInfo);
}

//change address
exports.updateOrderAddress = (oid) => {
    return Order.findByIdAndUpdate(oid, { orderAddress: orderAddress }).select(orderAddress).exec();
}

//delete order
exports.remove = (oid) => {
    return Order.findByIdAndRemove(oid).exec();
}
