const createError = require('http-errors');
const express = require('express');
const path = require('path');
const logger = require('morgan');
const mongoose = require('mongoose');
const session = require ('express-session');
const redis = require('redis');
const RedisStore = require('connect-redis')(session);

const config = require('./utils/config');

const indexRouter = require('./routes/index');
const apiRouter = require('./routes/api');

const app = express();

// connect to mongoDB
mongoose.connect(config.mongodb);
const db = mongoose.connection;
db.on('connected', () => { console.log('Connected to MongoDB.') });
db.on('error', () => { console.log('Error connecting to MongoDB.') });
//connect to redis(session)
const redisClient = redis.createClient(config.redis.port, config.redis.server); 
redisClient.on("error", function(error) {
    console.log(error);// redis error connection
});

// Resolve cross-domain request issues
app.all('*', function (req, res, next) {
  res.header('Access-Control-Allow-Credentials', true);
  res.header('Access-Control-Allow-Origin', req.headers.origin);
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,UPDATE,OPTIONS');
  res.header('Access-Control-Allow-Headers', 'X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept');
  next();
});


////Use the session middleware
const identityKey = 'skey';
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(session({ 
    store: new RedisStore({client: redisClient}),// store: new FileStore(),
    name: identityKey, //设置cookie中保存seesionID的名字
    secret: 'keyboardcat', // used to sign name for cookie that is related to session id
    resave: false,
    saveUninitialized: false, //require permission before setting a cookie, reduce server storage usage.
    cookie: { 
      maxAge: 60000000,
      domain: '',
      httpOnly: false,
     },
}));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/api', apiRouter);

// catch 404 and forward to error handler
app.use((req, res, next) => {
  next(createError(404));
});

// error handler
app.use((err, req, res, next) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  res.status(err.status || 500).json(err);
});

module.exports = app;
