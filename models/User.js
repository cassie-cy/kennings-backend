const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const addressSchema = new Schema({
    forename: String,
    surname: String,
    country: String,
    flatNumber: String,
    streetAddress: String,
    address2: String,
    city: String,
    postcode: String,
});

const paymentSchema = new Schema({
    cardNumber: String,
    name: String,
    expiryDate: String,
    cardType: String,
    billingAddress: addressSchema,
});

const User = mongoose.model('user', new Schema({
    username: {
        type: String,
        unique: true,
        required: true,
    },
    password: {
        type: String,
        required: true,
    },
    forename: {
        type: String,
        required: true,
    },
    surname: {
        type: String,
        required: true,
    },
    mobile: {
        type: String,
        required: true,
    },
    addresses: {
        type: [addressSchema],
        default: [],
        required: true,
    },
    paymentMethods: {
        type: [paymentSchema],
        required: true,
        default: [],
    },
    contactPreference: {
        type: {
            email: Boolean,
            SMS: Boolean,
            post: Boolean,
        }
    },
}));
//user register
exports.create = (info) => User.create(info);

// return all informations about one user (except password?)
exports.read = (_id) => User.findById(_id).exec();

// check username available 
exports.readByUsername = (username) => User.findOne({ username }).exec();

// update personal information(except password)
exports.updateBasicProfile = (_id, updates) => User
    .findByIdAndUpdate(_id, updates, { new: true }).exec();

// delete user?????????????
exports.delete = (_id) => User.findByIdAndRemove(_id).exec();

// add address
exports.addAddress = (_id, address) => User
    .findByIdAndUpdate(_id, { '$push': { addresses: address } }, { new: true })
    .exec();

// update address
exports.updateAddress = (_id, addressId, updates) => User
    .findOneAndUpdate(
        { _id, 'addresses._id': addressId },
        { '$set': { 'addresses.$': updates } }, { new: true }
    ).exec();

// delete address
exports.deleteAddress = (_id, addressId) => User
    .findByIdAndUpdate(
        _id,
        { '$pull': { addresses: { _id: addressId } } },
        { new: true }
    ).exec();

// add payment
exports.addPayment = (_id, payment) => User
    .findByIdAndUpdate(_id, { '$push': { paymentMethods: payment } }, { new: true })
    .exec();

//update payment
exports.updatePaymnet = (_id, paymentId, updates) => User
    .findOneAndUpdate(
        { _id, 'paymentMethods._id': paymentId },
        { '$set': { 'paymentMethods.$': updates } }
    ).exec();

//delete payment
exports.deletePayment = (_id, paymentId) => User
    .findByIdAndUpdate(
        _id,
        { '$pull': { paymentMethods: { _id: paymentId } } },
        { new: true }
    ).exec();