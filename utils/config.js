const config = {
    mongodb: 'mongodb://localhost:27017/kennings',
    redis: {
        server: '127.0.0.1',
        port: 6379,
    }
}

module.exports = config;

