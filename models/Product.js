const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Product = mongoose.model('product', new Schema({
    name: {                 // name of the product
        type: String,
        required: true,
    },
    details: {              // details of the product 
        type: String,
        required: true,
    },
    price: {                // price of the product
        type: Number,
        required: true,
    },
    quantity: {             // the number of in-stock product
        type: Number,
        required: true,
    },
    code: {                 // the product code
        type: String,
        required: true,
    },
    tecInfo: {              // technical specifications of the product
        type: String,
    },
    review: [{              // user reviews of the product
        body: String,
        date: String,
        usertitle: String,
    }],
    category: {             // the category of the product
        type: String,
        required: true,
        index: true,
    },
    picture: {              // the path to the picture of the product
        type: String,
        required: true,
    },
}));

//add product
exports.createProduct = (productInfo) => {
    return Product.create({
        ...productInfo,
        productTecInfo,
        productReview,
    })
}

//get product category
exports.getProductCategory = (category) => {
    return Product.find({category: category}).exec();
}
//get product information
exports.getProductInfo = (product_id) => {
    return Product.findById(product_id).exec();
}
//get products information
exports.getProductsInfo = (product_id) => {
    return Product.find().where('_id').in(product_id).exec();
}

//change information
exports.updateName = (id, name) => {
    return Product.findByIdAndUpdate(id, { name: name }, { new: true }).select('name').exec();
}

exports.updateDetails = (id, details) => {
    return Product.findByIdAndUpdate(id, { details: details }, { new: true }).select('details').exec();
}

exports.updatePrice = (id, price) => {
    return Product.findByIdAndUpdate(id, { price: price }, { new: true }).select('price').exec();
}

exports.updateQuantity = (id, quantity) => {
    return Product.findByIdAndUpdate(id, { quantity: quantity }, { new: true }).select('quantity').exec();
}

exports.updateTecInfo = (id, tecInfo) => {
    return Product.findByIdAndUpdate(id, { tecInfo: tecInfo }, { new: true }).select('tecInfo').exec();
}

exports.updateReview = (id, review) => {
    return Product.findByIdAndUpdate(id, { review: review }, { new: true }).select('review').exec();
}

exports.updateCode = (id, code) => {
    return Product.findByIdAndUpdate(id, { code: code }, { new: true }).select('code').exec();
}

exports.updateCategory = (id, category) => {
    return Product.findByIdAndUpdate(id, { category: category }, { new: true }).select('category').exec();
}

exports.search = (keyword) => {
    const keywordReg = new RegExp(keyword, 'i');
    return Product.find({ name: keywordReg }).exec();
}