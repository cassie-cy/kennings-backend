var express = require('express');
var router = express.Router();

router.get('/', (req, res) => {
  res.status(200).json({ message: 'test' });
}); 

router.get('/test/:test', (req, res) => {
  const test = req.params.test;
  res.status(200).json({ message: test });
});

module.exports = router;
